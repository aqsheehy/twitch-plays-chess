// Handles the connection to the twitch chat.

var irc = require("irc");
var processor = require('./processor');
var log = require('./log');
var game = require('./game');
var _ = require('underscore');
var parser = require('./parser');

var settings = {
    channels : ["#twitchtrieschess", "#semicolonoscopy", "#twitchplayssalem"],
    server : "irc.twitch.tv",
    port: 6667,
    secure: false,
    nick : "twitchtrieschess",
    password : "oauth:qikd0e50v5xa0yd6hb9s16to4iwsig" 
};

var ircContainer = {};
var backers = {};

var bot = null;

ircContainer.start = function(){

	bot = new irc.Client(settings.server, settings.nick, {
	    channels: [settings.channels + " " + settings.password],
	    debug: false,
	    password: settings.password,
	    username: settings.nick
	});

	bot.connect();

	bot.addListener('message', function (from, to, message) {

		var parsed = parser.Parse(message);
		if (!parsed) return;
		processor.push(parsed);
		
	});

	bot.addListener('error', log.error);

	processor.addListener('gameStarted', function(){
		bot.say('#twitchtrieschess', "Starting a new game...")
	});

	processor.addListener('moveMade', function(move){
		bot.say('#twitchtrieschess', "Performing move: " + move);
	});

	processor.addListener('opponentMoved', function(move){
		game.allPossibleMoves(function(moves){
			bot.say('#twitchtrieschess', "Opponent moved: " + move);
			bot.say('#twitchtrieschess', "Possible moves are: " + moves.join(', '));
		});
	});
};

module.exports = ircContainer;