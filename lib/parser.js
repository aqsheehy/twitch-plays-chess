function Parse(text){

	if (!text || text[0] != '!') return null;

	var parts = text.split(' ');
	var name = parts[0].toLowerCase();

	switch(name){
		case '!start':
			return ['!start', []];
		default:
			return ['!move', [ parts[0].slice(1) ]];

	}

}


module.exports = { Parse: Parse };