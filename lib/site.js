var log = require('./log');
var webdriverio = require('webdriverio');

var client = webdriverio.remote({ desiredCapabilities: { browserName: 'chrome' } }).init();
var site = {};

var username = 'TwitchTriesChess';
var password = 'Tyberzann02!'; // Hello reader.

function __open(closeExisting){

	return client
		.url('http://en.lichess.org/login', log.error)
	    .setValue('#username', username, log.error)
	    .setValue('#password', password, log.error)
	    .submitForm(".signup_box form", log.error)
	    .setViewportSize({ width: 1600, height: 800 }, log.error);

}

site.start = function(){
	__open();
};

site.newGame = function(fn){

	delete site._color;

	client.endAll();
	client = webdriverio.remote({ desiredCapabilities: { browserName: 'chrome' } }).init();

	__open()
		.url('http://en.lichess.org/setup/hook', log.error)
		.waitFor('button.random', 5000)
		.click('button.random', fn);
};

site.getColor = function(fn){

	if (site._color) return fn(site._color);

	client.getText("#site_header .user_link", function(err, users){
		if (err) log.error(err);
		if (!users) return fn(null);
		site._color = users[0].indexOf('TwitchTriesChess ') > -1 ? "w" : "b";
		fn(site._color);
	});

};

site.getComplete = function(fn, dump){

	if (dump) delete site._complete;
	if (site._complete) return fn(site._complete);

	client.isExisting('#site_header .status', function(err, status){
		if (status) return fn(true);
		client.isExisting('#site_header .players', function(err, players){
			if (!players) return fn(true);
			client.isExisting('.force_resign_zone', function(err, resignable){
				fn(resignable);
			});
		});
	});

};

site.getMoves = function(fn) {

	client.getText("#lichess .move", function(err, moves){

		moves = moves || [];

		if (typeof moves == typeof "")
			moves = [moves];

		return fn(moves);

	});

};

site.move = function(from, to, promotion, fn){

	var then = client
		.click('#lichess .' + from, log.error)
		.click('#lichess .' + to, promotion ? log.error : fn);

	if (promotion)
		then.waitFor('#promotion_choice', 1000, log.error)
			.click( _promotionClass(promotion), fn );

};

function _promotionClass(promotion){
	switch (promotion){
		case 'q': return '#promotion_choice .queen';
		case 'b': return '#promotion_choice .bishop';
		case 'r': return '#promotion_choice .rook';
		case 'n': return '#promotion_choice .knight';
		case 'k': return '#promotion_choice .king';
		default: return '#promotion_choice .queen';
	}
}

module.exports = site;