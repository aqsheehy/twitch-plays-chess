// Batch processor for cached IRC messages.

var log = require('./log');
var game = require('./game');

var processor = {};

processor.handlers = {};
processor.messagePool = [];

processor.start = function(){
    _queueStateCheck();
    _queueSweep();
};

processor.sweep = function(next){

    var randomIndex = Math.floor(Math.random()*processor.messagePool.length);
    var message = processor.messagePool[randomIndex];

    if (!message) return next();
    
    var type = message[0];
    var args = message[1];

    log.info('------ Sweeping: ' + type);
    
    switch(type)
    {
        case '!start':
            processor.messagePool = [];

            game.newGame(function(err){
                if (err) {
                    processor.messagePool.splice(randomIndex);
                    return processor.sweep(next);
                }
                processor.notify("gameStarted", null);
                next();
            });

            break;
        case '!move':
            game.move(args[0], function(err){
                if (err) {
                    processor.messagePool.splice(randomIndex);
                    return processor.sweep(next);
                }
                processor.notify("moveMade", args[0]);
                processor.messagePool = [];
                next();
            });
            break;
    }

};

processor.push = function(parsed, count){

    count = count || 1;

    var type = parsed[0];
    var args = parsed[1];

    log.info('++++++ Pushed: ' + type);

    switch (type){
        case '!start':
            game.getComplete(function(complete){
                if (complete) 
                    processor.messagePool = [ parsed ];
            });
            break;
        case '!move':
            game.canMove(function(canMove){
                if (!canMove) return;
                game.allPossibleMoves(function(moves){
                    if (moves.indexOf(args[0]) > -1) 
                        _addToQueue(parsed, count);
                });
            });
            break;
    }

};

processor.addListener = function(type, fn){
    processor.handlers[type] = fn;
};

processor.notify = function(type, value){
    processor.handlers[type](value || null);
};

function _addToQueue(message, count){
    count = count || 1;
    for (var i = 0; i < count; i++){
        processor.messagePool.push(message);
    }
}

function _queueStateCheck(){

    setTimeout(function(){
        game.loadMoves(function(unaccounted){
            if (unaccounted && unaccounted.length > 0)
                processor.notify("opponentMoved", unaccounted[0]);
            _queueStateCheck();
        });
    }, 5000);

};

function _queueSweep(){

    setTimeout(function(){
        processor.sweep(_queueSweep);
    }, 2000);

};

module.exports = processor;