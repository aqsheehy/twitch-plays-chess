// Handles requested state changes and validates their logic against the state.

var site = require('./site');
var chess = require('./chess');
var log = require('./log');

var game = {};

var board = null;

game.getComplete = function(fn){
	site.getComplete(fn);	
};

game.allPossibleMoves = function(fn){
	fn(board.moves());
};

game.canMove = function(fn){

	site.getComplete(function(complete){

		if (complete) return fn(false);

		site.getColor(function(color){

			fn(board && board.turn() == color);

		});

	});

};

game.newGame = function(fn){

	site.getComplete(function(complete){

		if (!complete) return fn();

		log.info("!!!!!! New game");

		site.newGame(function(){
			board = new chess.Chess();
			fn(); 
		});

	}, true);

};

game.move = function(mv, fn){

	game.canMove(function(canMove){

		if (!canMove) return fn("cannotMove");

		var desc = board.move(mv);
		if (!desc) return fn("invalidMove");

		log.info("!!!!!! Moving: " + JSON.stringify(desc));

		site.move(desc.from, desc.to, desc.promotion, fn);

	});

};

game.loadMoves = function(fn){

	site.getComplete(function(complete){

		if (complete || !board) {
			log.info('Not getting moves...');
			return fn(null);
		}

		log.info('Getting moves...');

		site.getMoves(function(moves){

			var history = board.history();

			if (!moves || moves.length <= history) return fn(null);

			var unaccounted = moves.slice(history.length);

			if (unaccounted.length == 0) return fn(null);

			for (var i = 0; i < unaccounted.length; i++){
				var desc = board.move(unaccounted[i]);
				log.info("!!!!!! Unaccounted: " + JSON.stringify(desc));
			}

			fn(unaccounted);

		});

	}, true);

};

module.exports = game;