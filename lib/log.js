var EventLogger = require('node-windows').EventLogger;
var log = new EventLogger('Twitch Tries Chess');

module.exports = {
	error: function(err){
		if (!err) return;
		console.log(err);
		log.error(err.toString());
	},
	info: function(info){
		console.log(info);
	}
};