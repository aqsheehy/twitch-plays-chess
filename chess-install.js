var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'chess',
  description: 'Hosts the IRC -> selenium bridge',
  script: require('path').join(__dirname,'chess.js')
});

svc.on('install',function(){
	svc.start();
});

svc.on('alreadyinstalled', function(){
	console.log('Already installed!');
});

svc.on('invalidinstallation', function(){
	console.log('Invalid installation!');
});

svc.install();